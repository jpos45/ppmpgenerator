# PPMPGenerator

## What is it

This Project is an simple to use PPMPGenerator. It generates all types of payloads and sends it over mqtt to a server. It uses the unide-python library from eclipse unide

The PPMPGeneratror provides a GUI but also works on the console.

## How to use it

To use it you have to give it values for the iterations, the type of payload and the frequenz of sending payloads to server.

- iterations: 0 - 999999 (0 stands for infinity)  
- payloadtypes: 1 - 5 (1:Messagepayload, 2:Measurementpayload, 3:Processpayload, 4:All type of payload, 5:Randompayload)  
- frequenz: 1 - 999999 (seconds)  

## What the f*** there is a Debugmode

For more output on the console or in the output field switch to debugmode. Hardcoded set the debug-flag in script or via the editmenu in the GUI.

Have fun :)