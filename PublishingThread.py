import threading
import config
import paho.mqtt.client as mqtt
import time
import inspect
import PPMPGenerator


class PublishingThread(threading.Thread):

    def __init__(self, controller, name='PublishingThread', debug=False):
        """ constructor, setting initial variables """
        self.controller = controller

        self.iterations = "1"
        self.ptype = "1"
        self.frequenz = "1"

        self._stopevent = threading.Event()

        threading.Thread.__init__(self, name=name)
        self.debug = debug
        self.ppmp_generator = PPMPGenerator.PPMPGenerator(self.controller, self.debug)

        self.options = {
            1: self.ppmp_generator.create_message,
            2: self.ppmp_generator.create_measurement,
            3: self.ppmp_generator.create_process,
            4: self.ppmp_generator.create_all,
            5: self.ppmp_generator.create_random
        }

        self.host = config.server
        self.port = config.port
        self.client = mqtt.Client()

    def setParams(self, i="1", t="1", f="1"):
        self.iterations = i
        self.ptype = t
        self.frequenz = f

    @staticmethod
    def lineno():
        return inspect.currentframe().f_back.f_lineno

    def publish(self):
        if self.debug:
            print("DEBUG: line " + str(self.lineno()) + ": Publishing")
            self.controller.writeToText("DEBUG: line " + str(self.lineno()) + ": Publishing")

        payload = self.options[int(self.ptype)]()
        if isinstance(payload, tuple):
            print("sending message payload")
            self.controller.writeToText("sending message payload")
            self.client.publish("test/test_1", payload[0])
            time.sleep(int(self.frequenz))
            print("sending measurement payload")
            self.controller.writeToText("sending measurement payload")
            self.client.publish("test/test_1", payload[1])
            time.sleep(int(self.frequenz))
            print("sending process payload")
            self.controller.writeToText("sending process payload")
            self.client.publish("test/test_1", payload[2])
            time.sleep(int(self.frequenz))
        else:
            print("sending payload")
            self.controller.writeToText("sending payload")
            self.client.publish("test/test_1", payload)
            time.sleep(int(self.frequenz))

    def on_connect(self, client, userdata, flags, rc):
        if self.debug:
            print("DEBUG: line " + str(self.lineno()) + ":")
            self.controller.writeToText("DEBUG: line " + str(self.lineno()) + ":")
        print("Connected with result code " + str(rc))
        self.controller.writeToText("Connected with result code " + str(rc))

        client.subscribe("test/test_2")

    def on_message(self, client, userdata, msg):
        if self.debug:
            print("DEBUG: line " + str(self.lineno()) + ": Got a message")
            self.controller.writeToText("DEBUG: line " + str(self.lineno()) + ": Got a message")
        print(msg.topic + ": " + str(msg.payload))
        self.controller.writeToText(msg.topic + ": " + str(msg.payload))

    def connect(self):
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message

        self.client.connect(self.host, self.port, 60)
        if self.debug:
            print(
                "DEBUG: line " + str(self.lineno()) + ": Connect to " + str(self.host) + " over port " + str(self.port))
            self.controller.writeToText(
                "DEBUG: line " + str(self.lineno()) + ": Connect to " + str(self.host) + " over port " + str(self.port)
            )
        print(
            "Connect to " + str(self.host) + " over port " + str(self.port))
        self.controller.writeToText(
            "Connect to " + str(self.host) + " over port " + str(self.port)
        )
        return True if self.client else False

    def disconnect(self):
        self.client.disconnect()
        if self.debug:
            print("DEBUG: line " + str(self.lineno()) + ": Client disconnected")
            self.controller.writeToText("DEBUG: line " + str(self.lineno()) + ": Client disconnected")
        print("Client disconnected")
        self.controller.writeToText("Client disconnected")

    def run(self):
        """ main control loop """
        if not self.connect():
            self._stopevent.set()

        if int(self.iterations) != 0 and not self._stopevent.is_set():
            self.client.loop_start()
            i = int(self.iterations)
            while i > 0:
                if self._stopevent.is_set():
                    break
                self.publish()
                i -= 1
            self.client.loop_stop()
            self._stopevent.set()
        else:

            while not self._stopevent.is_set():
                self.client.loop_forever()
                self.publish()

        self.disconnect()

    def join(self, timeout=None):
        """ Stop the thread. """
        threading.Thread.join(self, timeout)
        self._stopevent.set()
