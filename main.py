import inspect
from tkinter import *

import PublishingThread


class PPMPGeneratorGUI:

    def __init__(self, master, controller):

        self.controller = controller

        self.master = master

        self.menubar = Menu(self.master, bg="grey", fg="white")

        self.filemenu = Menu(self.menubar, tearoff=0)
        self.filemenu.add_command(label="Exit", command=self.master.quit)
        self.menubar.add_cascade(label="File", menu=self.filemenu)

        self.var = BooleanVar()

        self.editmenu = Menu(self.menubar, tearoff=0)
        self.editmenu.add_checkbutton(
            label="Debug",
            onvalue=True,
            offvalue=False,
            variable=self.var,
            command=self.controller.invert_debug)
        self.menubar.add_cascade(label="Edit", menu=self.editmenu)

        self.master.config(menu=self.menubar)

        self.input_frame = Frame(master=self.master)
        self.input_frame.pack(pady=10)

        # valid percent substitutions (from the Tk entry man page)
        # note: you only have to register the ones you need; this
        # example registers them all for illustrative purposes
        #
        # %d = Type of action (1=insert, 0=delete, -1 for others)
        # %i = index of char string to be inserted/deleted, or -1
        # %P = value of the entry if the edit is allowed
        # %s = value of entry prior to editing
        # %S = the text string being inserted or deleted, if any
        # %v = the type of validation that is currently set
        # %V = the type of validation that triggered the callback
        #      (key, focusin, focusout, forced)
        # %W = the tk name of the widget

        vcmd = (self.master.register(self.onValidate),
                '%i', '%S', '%W')

        self.iterations_label = Label(self.input_frame, text="Iterations")
        self.iterations_label.grid(row=0, column=0, sticky=W)
        self.iterations_text = Entry(self.input_frame, name="iter_text", width=20, validate="key", validatecommand=vcmd)
        self.iterations_text.grid(row=0, column=1)
        self.iterations_text.insert(END, "1")

        self.payloadtype_label = Label(self.input_frame, text="Payloadtype")
        self.payloadtype_label.grid(row=1, column=0, sticky=W)
        self.payloadtype_text = Entry(self.input_frame, name="pload_text", width=20, validate="key",
                                      validatecommand=vcmd)
        self.payloadtype_text.grid(row=1, column=1)
        self.payloadtype_text.insert(END, "1")

        self.frequenz_label = Label(self.input_frame, text="Frequenz")
        self.frequenz_label.grid(row=2, column=0, sticky=W)
        self.frequenz_text = Entry(self.input_frame, name="freq_text", width=20, validate="key", validatecommand=vcmd)
        self.frequenz_text.grid(row=2, column=1)
        self.frequenz_text.insert(END, "1")

        self.button_frame = Frame(master=self.master)
        self.button_frame.pack()

        self.go = Button(
            master=self.button_frame,
            text="Start",
            command=lambda: controller.start(
                self.iterations_text.get(),
                self.payloadtype_text.get(),
                self.frequenz_text.get()),
            bg="grey",
            fg="white")
        self.go.grid(row=0, column=0)
        self.stop = Button(master=self.button_frame, text="Stop", command=controller.stop, bg="grey", fg="white")
        self.stop.grid(row=0, column=1)

        self.output_frame = Frame(master=self.master, width=50, height=20)
        self.output_frame.pack(padx=10, pady=10)

        self.output_text = Text(master=self.output_frame)
        self.output_text.grid(row=0, column=0, sticky="nsew")

        self.scrollbar = Scrollbar(self.output_frame, command=self.output_text.yview)
        self.scrollbar.grid(row=0, column=1, sticky="nsew")

        self.output_text['yscrollcommand'] = self.scrollbar.set

        self.master.title("PPMPGenerator")

    def onValidate(self, i, S, W):

        self.controller.writeToText(W)

        options = {
            ".!frame.iter_text": [r"[0-9]", 8],
            ".!frame.pload_text": [r"[1-5]", 1],
            ".!frame.freq_text": [r"[0-9]", 8]
        }

        if re.match(options[W][0], S) and int(i) < options[W][1]:
            return True
        else:
            return False


class PPMPGeneratorController:
    gui = None

    def __init__(self):
        self.debug = False

        self.iterations = "1"
        self.type = "1"
        self.frequenz = "1"

        choosegui = input("Wollen Sie die grafische Benutzeroberflaeche starten? (y/n)")
        while not re.match(r"^([YyJjNn])$", choosegui):
            choosegui = input("")

        if re.match(r"^([YyJj])$", choosegui):
            self.root = Tk()
            self.gui = PPMPGeneratorGUI(self.root, self)
            self.root.mainloop()

        else:

            self.iterations = input("Wie oft soll ein Payload gesendet werden: ")
            while not re.match(r"^([0-9]{0,7})$", self.iterations):
                self.iterations = \
                    input("Bitte eine Zahl von 1-999999 oder 0 fuer unendliche viele Iterationen eingeben: ")

            self.ptype = \
                input("Geben sie den Type des Payloads an (1:Message|2:Measurement|3:Process|4:all|5:random): ")
            while not re.match(r"^([1-5])$", self.ptype):
                self.ptype = \
                    input("Bitte geben Sie eine Zahl zwischen 1-3 ein "
                          + "(1:Message|2:Measurement|3:Process|4:all|5:random): ")

            self.frequenz = \
                input("Geben Sie die Frequenz ein, in der die Payloads gesendet werden sollen (in Sekunden): ")
            while not re.match(r"^([1-9][0-9]*)$", self.frequenz):
                self.frequenz = \
                    input("Bitte geben Sie eine Ganzzahl ein: ")

            self.t = PublishingThread.PublishingThread(self, debug=self.debug)
            self.t.setParams(self.iterations, self.ptype, self.frequenz)
            self.t.start()

    @staticmethod
    def lineno():
        return inspect.currentframe().f_back.f_lineno

    def start(self, i, payloadtype, freq):
        if self.debug:
            print("DEBUG: line " + str(self.lineno()) + ": start clicked")
            self.writeToText("DEBUG: line " + str(self.lineno()) + ": start clicked")
        self.t = PublishingThread.PublishingThread(self, debug=self.debug)
        self.t.setParams(i, payloadtype, freq)
        self.t.start()

    def stop(self):
        if self.debug:
            print("DEBUG: line " + str(self.lineno()) + ": stop clicked")
            self.writeToText("DEBUG: line " + str(self.lineno()) + ": stop clicked")
        self.t.join()

    def invert_debug(self):
        self.debug = not self.debug

    def writeToText(self, text):
        if self.gui:
            self.gui.output_text.insert(END, "\n" + str(text))


if __name__ == "__main__":
    c = PPMPGeneratorController()
