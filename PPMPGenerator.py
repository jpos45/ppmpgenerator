import unide as unide
import inspect

from random import randint
from unide.util import dumps
from unide.process import (ProcessPayload, Part, Process, Program, Measurement)


class PPMPGenerator(object):

    def __init__(self, controller, debug=False):
        self.debug = debug
        self.controller = controller

        self.options = {
            1: self.create_message,
            2: self.create_measurement,
            3: self.create_process
        }

    @staticmethod
    def lineno():
        return inspect.currentframe().f_back.f_lineno

    def create_device(self, payload):
        if self.debug:
            print("DEBUG: line " + str(self.lineno()) + ": in createDevice(" + payload + ")")
            self.controller.writeToText()
        return unide.common.Device(payload + "Device")

    def create_message(self):
        if self.debug:
            print("DEBUG: line " + str(self.lineno()) + ": in create_message()")
            self.controller.writeToText("DEBUG: line " + str(self.lineno()) + ": in create_message()")
        device = self.create_device("message")
        return device.message("Info: Test")

    def create_measurement(self):
        if self.debug:
            print("DEBUG: line " + str(self.lineno()) + ": in create_measurement()")
            self.controller.writeToText("DEBUG: line " + str(self.lineno()) + ": in create_measurement()")
        device = self.create_device("measurement")
        return device.measurement(temperature=36.7)

    def create_process(self):
        if self.debug:
            print("DEBUG: line " + str(self.lineno()) + ": in create_process()")
            self.controller.writeToText("DEBUG: line " + str(self.lineno()) + ": in create_process()")
        device = self.create_device("process")

        process = Process(externalProcessId="b4927dad-58d4-4580-b460-79cefd56775b")

        payload = ProcessPayload(
            device=device,
            process=process)

        return dumps(payload, indent=4)


    def create_all(self):
        if self.debug:
            print("DEBUG: line " + str(self.lineno()) + ": in create_all()")
            self.controller.writeToText("DEBUG: line " + str(self.lineno()) + ": in create_all()")
        return self.create_message(), self.create_measurement(), self.create_process()

    def create_random(self):
        if self.debug:
            print("DEBUG: line " + str(self.lineno()) + ": in create_random()")
            self.controller.writeToText("DEBUG: line " + str(self.lineno()) + ": in create_random()")
        return self.options[randint(1, 3)]()
